﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using LiveStreamingTunisia.iOS;

using AVFoundation;
using Foundation;
using UIKit;
using CoreGraphics;
using AVKit;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;

[assembly: ExportRenderer(typeof(ContentView), typeof(VideoPlayer_CustomRenderer))]

namespace LiveStreamingTunisia.iOS
{
	public class VideoPlayer_CustomRenderer : ViewRenderer
	{
		//globally declare variables
		AVAsset _asset;
		AVPlayerItem _playerItem;
		AVPlayer _player;

		AVPlayerLayer _playerLayer;
		UIButton playButton;

		protected override void OnElementChanged (ElementChangedEventArgs<View> e)
		{
			base.OnElementChanged (e);

			//Get the video
			//bubble up to the AVPlayerLayer

			var url = new NSUrl (LandingPage.buildUrl(GetIPAddress()));
			_asset = AVAsset.FromUrl (url);

			_playerItem = new AVPlayerItem (_asset);

			_player = new AVPlayer (_playerItem);

			_playerLayer = AVPlayerLayer.FromPlayer (_player);

			//Create the play button
			playButton = new UIButton ();
			playButton.SetTitle ("Play Video", UIControlState.Normal);
			playButton.BackgroundColor = UIColor.Gray;

			//Set the trigger on the play button to play the video
			playButton.TouchUpInside += (object sender, EventArgs arg) => {
				_player.Play();
			};
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			//layout the elements depending on what screen orientation we are. 
			if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.Portrait) {
				playButton.Frame = new CGRect (0, NativeView.Frame.Bottom - 50, NativeView.Frame.Width, 50);
				_playerLayer.Frame = NativeView.Frame;
				NativeView.Layer.AddSublayer (_playerLayer);
				NativeView.Add (playButton);
			} else if (DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeLeft || DeviceHelper.iOSDevice.Orientation == UIDeviceOrientation.LandscapeRight) {
				_playerLayer.Frame = NativeView.Frame;
				NativeView.Layer.AddSublayer (_playerLayer);
				playButton.Frame = new CGRect (0, 0, 0, 0);
			}
		}

		public string GetIPAddress()
		{
			bool ip_found = false;
			int j = 0;
			string[] uris = { "http://ipecho.net/plain", "http://www.whatismypublicip.com/", "http://www.ip-adress.eu/", "http://www.showmemyip.com/" };

			while (!ip_found && j < uris.Length)
			{
				try
				{
					Uri uri = new Uri(uris[j]);
					HttpWebRequest hl = (HttpWebRequest)HttpWebRequest.Create(uri);

					Stream dataStream = hl.GetResponse().GetResponseStream();
					StreamReader reader = new StreamReader(dataStream);
					string ip_container = reader.ReadToEnd();

					Regex ip_regex = new Regex("([0-9]{1,3}[.]){3}.([0-9]{1,3})");

					if ((ip_found = ip_regex.IsMatch(ip_container)))
						return ip_regex.Match(ip_container).ToString();

				}
				catch { }

				j++;
			}
			return "";
		}
	}
}