﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace LiveStreamingTunisia
{
	public class LandingPage : ContentPage
	{
		ContentView videoPlayer;

		public LandingPage ()
		{
			videoPlayer = new ContentView {
				WidthRequest = App.ScreenWidth/2,
				HeightRequest = App.ScreenHeight/2,
			};

			Content = new StackLayout {
				Children = {
					videoPlayer
				}
			};
		}

		protected override void LayoutChildren (double x, double y, double width, double height)
		{
			//need to change the size of the ContentView for Landscape Orientation
			//This enables fullscreen capabilities in the Custom Renderer
			if (width > height) {
				//Landscape Orientation
				videoPlayer.WidthRequest = App.ScreenWidth;
				videoPlayer.HeightRequest = App.ScreenHeight;
			} else if (width < height) {
				//Portrait Orientation
				videoPlayer.WidthRequest = App.ScreenWidth/2;
				videoPlayer.HeightRequest = App.ScreenHeight/2;
			}
			base.LayoutChildren (x, y, width, height);
		}

		public static string buildUrl(string ip)
		{
			if (ip != "")
			{
				string encoding_key = "X00b411sKsAjV00uLsN00Za511B";
				Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
				string timeStamp = (unixTimestamp + 4 * 3600).ToString();
				string secret_Key = Convert.ToBase64String(MD5Core.GetHash(encoding_key + "/" + ip + "/" + timeStamp));

				secret_Key = secret_Key.Replace("+", "-");
				secret_Key = secret_Key.Replace("/", "_");
				while (secret_Key[secret_Key.Length - 1] == '=')
					secret_Key = secret_Key.Substring(0, secret_Key.Length - 1);

				return "https://cdn.streaminghd.tn/public/2020/" + secret_Key + "/" + timeStamp + "/index.m3u8";
			}
			else
			{
				// Gestion de l'erreur pas d'adresse ip retrouvee
				return "";
			}
		}
	}
}