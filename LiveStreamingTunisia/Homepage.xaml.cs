﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace LiveStreamingTunisia
{
	public partial class Homepage : ContentPage
	{
		public Homepage()
		{
			InitializeComponent();
			this.SetSizes();

		}
		private void SetSizes()
		{
			ConferenceLogo.WidthRequest = App.ScreenWidth;
			ConferenceLogo.HeightRequest = 256 * App.ScreenWidth / 376;
			buttontext.HeightRequest = 22 * App.ScreenWidth / 376;

			box.WidthRequest = 295 * App.ScreenWidth / 376;
			box.HeightRequest = 60 * App.ScreenWidth / 376;
		}
	}
}
